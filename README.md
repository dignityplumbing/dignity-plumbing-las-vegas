Dignity Plumbing was founded by a local general contractor who had a hard time finding a plumber to rely on. Dignity plumbing is set up to please the customer and to bring plumbers with Dignity, not pushy salesmen! We pride ourselves on bringing Dignity to the plumbing industry.

Address: 1306 West Craig Rd, Ste H, North Las Vegas, Nevada 89032, USA

Phone: 702-840-8910
